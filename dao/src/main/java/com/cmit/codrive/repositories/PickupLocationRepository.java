package com.cmit.codrive.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cmit.codrive.entities.PickupLocation;

@Repository
public interface PickupLocationRepository extends JpaRepository<PickupLocation, Long>, JpaSpecificationExecutor<PickupLocation>, Serializable {

}
