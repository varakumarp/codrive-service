package com.cmit.codrive.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cmit.codrive.entities.RidePartner;

@Repository
public interface RidePartnerRepository extends JpaRepository<RidePartner, Long>, JpaSpecificationExecutor<RidePartner>, Serializable {

}
