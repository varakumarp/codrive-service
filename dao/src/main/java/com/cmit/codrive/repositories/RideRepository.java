package com.cmit.codrive.repositories;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cmit.codrive.entities.Ride;


@Repository
public interface RideRepository extends JpaRepository<Ride, Long>, JpaSpecificationExecutor<Ride>, Serializable {

	@Query(value = "select r from Ride r left join fetch r.ridePartners rp where rp.partner.id = :userId and r.rideStatus.id=:status order by r.startTime",
			countQuery = "select count(*) from Ride r left join r.ridePartners rp where rp.partner.id = :userId and r.rideStatus.id=:status order by r.startTime")
	public Page<Ride> findAllRidesForUserByStatus(@Param(value = "userId") final Long userId, @Param(value = "status") final Long status, Pageable pageable);
}
