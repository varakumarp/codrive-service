package com.cmit.codrive.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@MappedSuperclass
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = -2087009356961590573L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name = "createdTimestamp")
	@CreationTimestamp
	private LocalDateTime createdTimestamp;
	
	@Column(name = "createdBy")
	private Long createdBy;
	
	@Column(name = "modifiedTimestamp")
	@UpdateTimestamp
	private LocalDateTime modifiedTimestamp;
	
	@Column(name = "modifiedBy")
	private Long modifiedBy;
		
	
}
