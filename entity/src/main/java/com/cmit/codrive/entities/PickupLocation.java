package com.cmit.codrive.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity(name="PickupLocation")
@Getter(value = AccessLevel.PUBLIC)
@Setter
public class PickupLocation extends BaseEntity {

	private static final long serialVersionUID = 1561379709533590488L;

	@Column(name="location")
	private String location;
	
	@Column(name="lat")
	private Double lattitude;
	
	@Column(name="\"long\"")
	private Double longitude;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if (getId() != null) {
			result = prime * result + (this.getId() == null ? 0 : this.getId().hashCode());
		} else {
			result = prime * result + (this.getLocation() == null ? 0 : this.getLocation().hashCode());			
		}

		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PickupLocation other = (PickupLocation) obj;
		if (this.getId() == null && other.getId() != null) {
				return false;
		} else if (!this.getLocation().equals(other.getLocation()))
			return false;
		return true;
	}
		
}
