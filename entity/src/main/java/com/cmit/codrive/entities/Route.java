package com.cmit.codrive.entities;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "Route")
@Getter(value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@NoArgsConstructor
public class Route extends BaseEntity {

	private static final long serialVersionUID = -1860918660747794939L;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="riderId")
	private User rider;
	
	@Column(name="origin")
	private String origin;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name="originLat")
	private Double originLattitude;
	
	@Column(name="originLong")
	private Double originLongitude;
	
	@Column(name="destinationLat")
	private Double destinationLattitude;
	
	@Column(name="destinationLong")
	private Double destinationLongitude;

	@Column(name="daysInWeek")
	private String daysInWeek;
	
	@Column(name="startDate")
	private LocalDateTime startDate;

	@Column(name="endDate")
	private LocalDateTime endDate;
	
	@Column(name="startTime")
	private LocalTime startTime;
	
	@Column(name="offeringSeats")
	private Integer offeringSeats;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="routeId", referencedColumnName = "id")
	private Set<PickupLocation> pickupLocations;
	
}
