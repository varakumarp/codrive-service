package com.cmit.codrive.valueobjects;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
public class BaseEntityVO implements Serializable {

	private static final long serialVersionUID = -2860994213410321695L;

	private Long id;
	
	private LocalDateTime createdTimestamp;
	
	private Long createdBy;
	
	private LocalDateTime modifiedTimestamp;
	
	private Long modifiedBy;

}
