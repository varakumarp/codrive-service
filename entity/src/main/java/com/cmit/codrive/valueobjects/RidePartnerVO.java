package com.cmit.codrive.valueobjects;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter(value = AccessLevel.PUBLIC)
@Setter
public class RidePartnerVO extends BaseEntityVO {

	private static final long serialVersionUID = 334612106118828082L;

	private RideVO rideVO;
	
	private UserVO partnerVO;
	
	private Boolean isRider;
}
