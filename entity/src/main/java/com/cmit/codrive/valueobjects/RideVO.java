package com.cmit.codrive.valueobjects;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
property = "id")
public class RideVO extends BaseEntityVO {

	private static final long serialVersionUID = 4774891044605655793L;

	private RouteVO route;

	@JsonIdentityReference(alwaysAsId = true)
	private RideStatusVO rideStatus;

	private LocalDateTime startTime;

	private LocalDateTime endTime;

//	@JsonIgnore
	private List<RidePartnerVO> ridePartners;
}
