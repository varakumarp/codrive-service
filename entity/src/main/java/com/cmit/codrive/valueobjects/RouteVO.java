package com.cmit.codrive.valueobjects;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter(value = AccessLevel.PUBLIC)
@Setter
public class RouteVO extends BaseEntityVO {

	private static final long serialVersionUID = 3093764563835644776L;

	private UserVO rider;
	
	private String origin;
	
	private String destination;
	
	private Double originLattitude;

	private Double originLongitude;
	
	private Double destinationLattitude;
	
	private Double destinationLongitude;
	
	private String daysInWeek;
	
	private LocalDateTime startDate;
	
	private LocalDateTime endDate;
	
	private LocalTime startTime;
	
	private Integer offeringSeats;
	
	private Set<PickupLocationVO> pickupLocations;
	
	
}
