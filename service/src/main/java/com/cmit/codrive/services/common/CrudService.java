package com.cmit.codrive.services.common;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.valueobjects.BaseEntityVO;

public interface CrudService <T extends BaseEntityVO, K> {

	@Transactional
	public T createEntity(UserContext userContext, T entity);

	@Transactional
	public T updateEntity(UserContext userContext, T entity) throws Exception;

	@Transactional
	public void deleteEntity(UserContext userContext, K id) throws Exception;

	@Transactional
	public T getEntity(UserContext userContext, K id);

	@Transactional
	public List<T> getEntityList(UserContext userContext);
	
	@Transactional
	public Page<T> getEntityList(UserContext userContext, T searchFilter, int page, int size);
	
}
