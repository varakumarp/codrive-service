package com.cmit.codrive.services.ride;

import org.springframework.data.domain.Page;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.RideVO;

public interface RideService extends CrudService<RideVO, Long> {
	Page<RideVO> getUpcomingRidesForUser(UserContext userContext, Integer page, Integer size);
	
	Page<RideVO> getCompletedRidesForUser(UserContext userContext, Integer page, Integer size);
}
