package com.cmit.codrive.services.ride;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import com.cmit.codrive.entities.UserContext;
import com.cmit.codrive.repositories.RideRepository;
import com.cmit.codrive.services.common.RideStatusConstant;
import com.cmit.codrive.valueobjects.RideVO;

@Service("rideService")
public class RideServiceImpl implements RideService {

	@Autowired
	private RideRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public RideVO createEntity(UserContext userContext, RideVO entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RideVO updateEntity(UserContext userContext, RideVO entity) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEntity(UserContext userContext, Long id) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public RideVO getEntity(UserContext userContext, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RideVO> getEntityList(UserContext userContext) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RideVO> getEntityList(UserContext userContext, RideVO searchFilter, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<RideVO> getUpcomingRidesForUser(UserContext userContext, @NonNull Integer page, @NonNull Integer size) {

		Page<RideVO> result = this.repository.findAllRidesForUserByStatus(userContext.getUser().getId(),
				RideStatusConstant.NEW.getValue(), PageRequest.of(page, size)).map((ride) -> {
					return this.mapper.map(ride, RideVO.class);
				});

		return result;
	}

	@Override
	public Page<RideVO> getCompletedRidesForUser(UserContext userContext, @NonNull Integer page,
			@NonNull Integer size) {
		Page<RideVO> result = this.repository.findAllRidesForUserByStatus(userContext.getUser().getId(),
				RideStatusConstant.COMPLETED.getValue(), PageRequest.of(page, size)).map((ride) -> {
					return this.mapper.map(ride, RideVO.class);
				});
		return result;
	}

}
