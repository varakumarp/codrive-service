package com.cmit.codrive.services.user;

import com.cmit.codrive.services.common.CrudService;
import com.cmit.codrive.valueobjects.UserVO;

public interface UserService extends CrudService<UserVO, Long> {

}
