/*
Created: 21-05-2020
Modified: 25-05-2020
Model: MySQL 8.0
Database: MySQL 8.0
*/

-- Create databases section -------------------------------------------------

CREATE DATABASE `CoDrive`;

-- Set default database -----------------------------------------------------
USE CoDrive;

-- Create tables section -------------------------------------------------

-- Table User

CREATE TABLE `User`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `firstName` Varchar(100) NOT NULL,
  `middleName` Varchar(100),
  `lastName` Varchar(100) NOT NULL,
  `email` Varchar(50) NOT NULL
  COMMENT 'Emaill id is not null and  unique',
  `mobile` Varchar(15) NOT NULL
  COMMENT 'Mobile number is not null and  unique',
  `isActive` Tinyint(1) NOT NULL DEFAULT 1,
  `externalSystemId` Varchar(10),
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `createdBy` Int,
  `modifiedBy` Int,
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship38` ON `User` (`createdBy`)
;

CREATE INDEX `IX_Relationship39` ON `User` (`modifiedBy`)
;

ALTER TABLE `User` ADD UNIQUE `mobile` (`mobile`)
;

ALTER TABLE `User` ADD UNIQUE `email` (`email`)
;

-- Table Ride

CREATE TABLE `Ride`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `routeId` Int NOT NULL,
  `startDateTime` Datetime NOT NULL,
  `endDateTime` Datetime,
  `rideStatusId` Int NOT NULL,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship11` ON `Ride` (`rideStatusId`)
;

CREATE INDEX `IX_Relationship20` ON `Ride` (`routeId`)
;

CREATE INDEX `IX_Relationship27` ON `Ride` (`createdBy`)
;

CREATE INDEX `IX_Relationship28` ON `Ride` (`modifiedBy`)
;

-- Table RideStatus

CREATE TABLE `RideStatus`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `code` Varchar(10) NOT NULL,
  `description` Varchar(50) NOT NULL,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship29` ON `RideStatus` (`createdBy`)
;

CREATE INDEX `IX_Relationship30` ON `RideStatus` (`modifiedBy`)
;

ALTER TABLE `RideStatus` ADD UNIQUE `code` (`code`)
;

-- Table RidePartner

CREATE TABLE `RidePartner`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `rideId` Int NOT NULL,
  `partnerId` Int NOT NULL,
  `isRider` Tinyint(1) NOT NULL DEFAULT 0,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship12` ON `RidePartner` (`rideId`)
;

CREATE INDEX `IX_Relationship13` ON `RidePartner` (`partnerId`)
;

CREATE INDEX `IX_Relationship33` ON `RidePartner` (`createdBy`)
;

CREATE INDEX `IX_Relationship34` ON `RidePartner` (`createdBy`)
;

CREATE INDEX `IX_Relationship35` ON `RidePartner` (`modifiedBy`)
;

-- Table Route

CREATE TABLE `Route`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `riderId` Int NOT NULL,
  `origin` Varchar(100) NOT NULL,
  `destination` Varchar(100) NOT NULL,
  `originLat` Decimal(10,8) NOT NULL,
  `originLong` Decimal(11,8) NOT NULL,
  `destinationLat` Decimal(10,8) NOT NULL,
  `destinationLong` Decimal(11,8) NOT NULL,
  `startDate` Datetime NOT NULL,
  `endDate` Datetime NOT NULL,
  `startTime` Time NOT NULL,
  `daysInWeek` Varchar(25) NOT NULL,
  `offeringSeats` Int NOT NULL,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship17` ON `Route` (`riderId`)
;

CREATE INDEX `IX_Relationship21` ON `Route` (`createdBy`)
;

CREATE INDEX `IX_Relationship22` ON `Route` (`modifiedBy`)
;

-- Table PickupLocation

CREATE TABLE `PickupLocation`
(
  `id` Int NOT NULL AUTO_INCREMENT,
  `routeId` Int,
  `location` Varchar(100) NOT NULL,
  `lat` Decimal(10,8) NOT NULL,
  `long` Decimal(11,8) NOT NULL,
  `createdBy` Int NOT NULL,
  `createdTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `modifiedBy` Int NOT NULL,
  `modifiedTimestamp` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
)
;

CREATE INDEX `IX_Relationship19` ON `PickupLocation` (`routeId`)
;

CREATE INDEX `IX_Relationship31` ON `PickupLocation` (`createdBy`)
;

CREATE INDEX `IX_Relationship32` ON `PickupLocation` (`modifiedBy`)
;

-- Create foreign keys (relationships) section -------------------------------------------------

ALTER TABLE `Ride` ADD CONSTRAINT `Relationship11` FOREIGN KEY (`rideStatusId`) REFERENCES `RideStatus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RidePartner` ADD CONSTRAINT `Relationship12` FOREIGN KEY (`rideId`) REFERENCES `Ride` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RidePartner` ADD CONSTRAINT `Relationship13` FOREIGN KEY (`partnerId`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Route` ADD CONSTRAINT `Relationship17` FOREIGN KEY (`riderId`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `PickupLocation` ADD CONSTRAINT `Relationship19` FOREIGN KEY (`routeId`) REFERENCES `Route` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Ride` ADD CONSTRAINT `Relationship20` FOREIGN KEY (`routeId`) REFERENCES `Route` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Route` ADD CONSTRAINT `Relationship21` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Route` ADD CONSTRAINT `Relationship22` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Ride` ADD CONSTRAINT `Relationship27` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `Ride` ADD CONSTRAINT `Relationship28` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RideStatus` ADD CONSTRAINT `Relationship29` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RideStatus` ADD CONSTRAINT `Relationship30` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `PickupLocation` ADD CONSTRAINT `Relationship31` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `PickupLocation` ADD CONSTRAINT `Relationship32` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RidePartner` ADD CONSTRAINT `Relationship33` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RidePartner` ADD CONSTRAINT `Relationship34` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `RidePartner` ADD CONSTRAINT `Relationship35` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `User` ADD CONSTRAINT `Relationship38` FOREIGN KEY (`createdBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;

ALTER TABLE `User` ADD CONSTRAINT `Relationship39` FOREIGN KEY (`modifiedBy`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
;




