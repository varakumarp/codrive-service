package com.cmit.codrive.web.common;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter(value = AccessLevel.PUBLIC)
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AuthenticationRequest implements Serializable {
	
	private static final long serialVersionUID = -8420625585944947124L;
	private String username;
    private String password;
}
