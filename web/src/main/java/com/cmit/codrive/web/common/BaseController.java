package com.cmit.codrive.web.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.cmit.codrive.entities.SecurityUser;
import com.cmit.codrive.entities.UserContext;

public class BaseController {

protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected UserDetails getUserDetails() {
		UserDetails userDetails = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			userDetails = (UserDetails) auth.getPrincipal();
		}
		return userDetails;
	}

	protected UserContext getUserContext() {
		SecurityUser user = (SecurityUser) getUserDetails();
		UserContext userContext = new UserContext();
		userContext.setAuthorities(user.getAuthorities());
		userContext.setUser(user.getUser());
		return userContext;
	}
}
