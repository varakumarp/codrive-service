package com.cmit.codrive.web.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebhrResponse {

	private String status;
	
	private String error;
	
	private ResponseData data; 
}

@Getter
@Setter
class ResponseData {
	private String firstName;
	private String token;
}
