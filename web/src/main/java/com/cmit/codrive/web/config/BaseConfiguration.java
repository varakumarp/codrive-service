package com.cmit.codrive.web.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseConfiguration {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();

	}
}
