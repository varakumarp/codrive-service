package com.cmit.codrive.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmit.codrive.services.user.UserService;
import com.cmit.codrive.valueobjects.UserVO;
import com.cmit.codrive.web.common.AuthenticationRequest;
import com.cmit.codrive.web.common.AuthenticationResponse;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;
import com.cmit.codrive.web.common.CustomUserDetailsService;
import com.cmit.codrive.web.common.LoginHelper;
import com.cmit.codrive.web.security.JwtUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin
public class LoginController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private LoginHelper helper;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse login(@RequestBody AuthenticationRequest request) {
		CommonResponse response = new CommonResponse();
		String token = null;
		String empId = null;

		// 1. Get the webHR token by authenticating user
		try {
			String webHrResponse = this.helper.authenticateRequest(request);
			JsonNode jsonNode = objectMapper.readTree(webHrResponse);

			token = jsonNode != null ? jsonNode.get("data").get("token").asText() : null;
			empId = jsonNode != null ? jsonNode.get("data").get("empid").asText() : null;

			if (token == null || (token != null && token.split("\\.").length != 3)) {
				throw new RuntimeException();
			}
		} catch (Exception e) {
			response.setStatus(false);
			response.setErrorCode(401);
			response.setErrorMessage("Unauthorized: Authentication failed");
			return response;
		}

		// 2. Get User details from WebHR api
		UserVO userVO = null;
		try {
			userVO = this.helper.getUserDetails(empId, token);
			this.userService.createEntity(null, userVO);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(false);
			response.setErrorCode(500);
			response.setErrorMessage("Error while saving the user details.");
			return response;
		}

		// 3. Generate JWT token
		try {
			final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());

			final String jwt = jwtUtil.generateToken(userDetails);
			AuthenticationResponse authResponse = new AuthenticationResponse();
			authResponse.setToken(jwt);
			response.setData(authResponse);

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(false);
			response.setErrorCode(500);
			response.setErrorMessage(e.getMessage());
		}

		return response;
	}

}