package com.cmit.codrive.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmit.codrive.services.ride.RideService;
import com.cmit.codrive.valueobjects.RideVO;
import com.cmit.codrive.web.common.BaseController;
import com.cmit.codrive.web.common.CommonResponse;

@RestController
@CrossOrigin
@RequestMapping(value = "rides")
public class RideController extends BaseController {

	@Autowired
	private RideService service;
	
	@RequestMapping(value="/upcoming", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getUpcomingRides(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size ) {
		CommonResponse response = new CommonResponse();
		
		try {
			Page<RideVO> rides = this.service.getUpcomingRidesForUser(getUserContext(), page, size);
			response.setData(rides);
				
		} catch(Exception e) {
			e.printStackTrace();
			response.setStatus(false);
			response.setErrorCode(500);
			response.setErrorMessage(e.getMessage());
		}
		
		return response;
	}

	@RequestMapping(value="/completed", method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse getCompletedRides(final @RequestParam("page") Integer page, final @RequestParam("size") Integer size ) {
		CommonResponse response = new CommonResponse();
		
		try {
			Page<RideVO> rides = this.service.getCompletedRidesForUser(getUserContext(), page, size);
			response.setData(rides);
		} catch(Exception e) {
			e.printStackTrace();
			response.setStatus(false);
			response.setErrorCode(500);
			response.setErrorMessage(e.getMessage());
		}
		
		return response;
	}

}
